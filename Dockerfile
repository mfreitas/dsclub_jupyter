#DSCLUB DOCKER IMAGE
#RELEASE 0.1
#
#BUILD
#docker build -t mfreitas/dsclub_jupyter:0.1 .
#
#RUN
#docker run -it -p 8888:8888 -v $PWD:/data \
#  --name notebo./run   okserver \
#  mfreitas/dsclub_jupyter:0.1 /bin/bash -c "jupyter notebook \
#  --ip 0.0.0.0 --no-browser --allow-root --NotebookApp.token='' \
#  --notebook-dir='/data'"
#
#STOP
#docker container stop notebookserver
#
#DELETE CONTAINER
#docker rm notebookserver
#
#DELTE IMAGE 
#docker rmi dsclub_jupyter:0.1
#
#CLEAN IMAGES AND CONTAINERS (DANGEROUS)
#docker container prune -f
#docker image prune -a

FROM ubuntu:18.04

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update --fix-missing && apt-get install -y wget bzip2 ca-certificates \
    libglib2.0-0 libxext6 libsm6 libxrender1 \
    git mercurial subversion

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-4.5.11-Linux-x86_64.sh -O ~/miniconda.sh
RUN /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

RUN conda config --add channels defaults &&  \
    conda config --add channels conda-forge && \
    conda config --add channels bioconda

COPY environment.yml environment.yml
RUN conda env update -f environment.yml
EXPOSE 8888
